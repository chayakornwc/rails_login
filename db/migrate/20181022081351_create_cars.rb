class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.belongs_to :user, foreign_key: true
      t.string :condition
      t.integer :year

      t.timestamps
    end
    add_index :cars, :user_id
  end
end
